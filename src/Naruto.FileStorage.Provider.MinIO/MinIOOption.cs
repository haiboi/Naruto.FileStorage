﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.FileStorage.Provider.MinIO
{
    public class MinIOOption
    {
        /// <summary>
        /// 
        /// </summary>
        public MinIOOption()
        {

        }
        /// <summary>
        /// 服务地址
        /// </summary>
        public string ServerUrl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 密钥
        /// </summary>
        public string Secret { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string BucketName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsSSL { get; set; }
    }
}
