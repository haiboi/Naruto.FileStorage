using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Naruto.FileStorage;
using Naruto.FileStorage.Provider.MinIO;
using Naruto.FileStorage.Provider.MongoDB;
using Naruto.FileStorage.Provider.Physical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddFileStorage(option =>
            {
                //option.UseMondbDBFileStorage(a =>
                //{
                //    a.ConnectionString = Configuration.GetSection("mongodb").GetValue<string>("dbstring");
                //    a.DataBase = Configuration.GetSection("mongodb").GetValue<string>("database");
                //});
                option.UseMinIO(a =>
                {
                    a.ServerUrl = "play.min.io";
                    a.Key = "Q3AM3UQ867SPQQA43P2F";
                    a.Secret = "zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG";
                    a.IsSSL = true;
                    a.BucketName = "test";
                });
            });
            var cors = Configuration.GetSection("cors").Get<string[]>();
            services.AddCors(a =>
            {
                a.AddDefaultPolicy(option =>
                {
                    option.AllowCredentials();
                    option.AllowAnyHeader();
                    option.AllowAnyMethod();
                    option.WithOrigins(cors);
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseCors();
            app.UseFileStorageMiddleware();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}
