﻿using Naruto.BaseMongo.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.FileStorage.Provider.MongoDB.Object
{
    /// <summary>
    /// 上传文件模型
    /// </summary>
    public class UploadFileModel : IMongoEntity
    {

        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName { get; set; }


        /// <summary>
        /// 文件的字节信息
        /// </summary>
        public byte[] FileBytes { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

    }
}
