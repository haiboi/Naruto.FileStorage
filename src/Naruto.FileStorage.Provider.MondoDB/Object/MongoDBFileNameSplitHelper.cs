﻿using Naruto.FileStorage.Object;
using Naruto.FileStorage.Provider.MongoDB.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.FileStorage.Helper
{
    /// <summary>
    /// 对文件名进行拆分
    /// </summary>
    public static class MongoDBFileNameSplitHelper
    {
        /// <summary>
        /// 根据 / 进行拆分 获取目录和文件名
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static (bool isBig, FileNameSplit fileNameSplit) MongoDBFileNameSplit(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return default;
            }
            var res = str.Split('/', StringSplitOptions.RemoveEmptyEntries);
            if (res != null && res.Length < 3)
            {
                return default;
            }
            return (res[0].ToLower() == MongoDBFileTypeEnum.BIG.ToString().ToLower() ? true : false, new FileNameSplit
            {
                DirectoryName = res[1],
                FileName = res[2]
            });
        }
    }
}
