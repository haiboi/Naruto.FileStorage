﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.FileStorage.Provider.MongoDB.Object
{
    internal sealed class MongoDBFileStorageConst
    {
        /// <summary>
        /// 小文件文件大小上限
        /// </summary>
        internal const int MaxFileLengthLimit = 15 * 1024 * 1024;
    }
}
