﻿using Naruto.FileStorage.Object;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.FileStorage.Provider.MongoDB.Interface
{
    public interface IMongoDBFile
    {
        /// <summary>
        /// 添加文件
        /// </summary>
        /// <param name="file">文件流</param>
        /// <param name="fileName">存储的文件名</param>
        /// <param name="fileStream">是否为大文件 >15M </param>
        /// <returns>返回文件请求路径</returns>
        Task<string> AddFileAsync(Stream fileStream, bool isBigFile, FileNameSplit fileName);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="isBigFile">是否为大文件</param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        Task DeleteFileAsync(bool isBigFile, FileNameSplit fileName);
        /// <summary>
        /// 获取文件
        /// </summary>
        /// <param name="isBigFile">是否为大文件</param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        Task<byte[]> GetFileAsync(bool isBigFile, FileNameSplit fileName);
    }
}
