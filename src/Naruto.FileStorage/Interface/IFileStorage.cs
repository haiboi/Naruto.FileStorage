﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.FileStorage.Abstractions
{
    /// <summary>
    /// 文件存储操作接口
    /// </summary>
    public interface IFileStorage
    {
        /// <summary>
        /// 添加文件
        /// </summary>
        /// <param name="file">文件流</param>
        /// <param name="fileName">文件地址</param>
        /// <returns>返回文件请求路径</returns>
        Task<string> AddFileAsync(Stream fileStream, string fileName);

        /// <summary>
        /// 文件删除
        /// </summary>
        /// <param name="fileName">文件地址</param>
        /// <returns></returns>
        Task DeleteFileAsync(string fileName);

        /// <summary>
        /// 获取文件
        /// </summary>
        /// <param name="fileName">文件地址</param>
        /// <returns></returns>
        Task<byte[]> GetFileAsync(string fileName);
    }
}
