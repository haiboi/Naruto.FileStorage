# 文件存储

#### 介绍
    文件存储，提供了两种方式的文件存储，一种是物理磁盘，一个是mongodb存储，用中间件的方式插入需要使用的系统中

#### 软件架构
    整个系统基于.net core 3.1 开发的


#### 使用说明

1.  整个系统的新增，删除，查看文件分别根据http谓语 ，PUT,DELETE,GET，来区分
2.  安装 <b>Naruto.FileStorage</b> nuget包
3.  使用的时候，首先要注入服务
```c#
      services.AddFileStorage(option =>
                        {
                        })
```
4.  当使用物理磁盘作为存储的话，还需要安装  <b>Naruto.FileStorage.Provider.Physical</b> nuget包,并且将服务注入
```c#
        services.AddPhyscalFileStorage();
```
5.  当使用mongodb作为存储的话，需要安装 <b>Naruto.FileStorage.Provider.MongoDB</b> nuget包,并且将服务注入
```c#
        services.AddMondbDBFileStorage();
```
6.  插入中间件
```c#
     option.UseFileStorageMiddleware();
```
7.  接口请求说明 使用前需要先在<b>AddFileStorage</b>中配置文件的请求地址,不填写默认地址前缀为<b>/naruto/file</b>，当调用<b>put</b>上传接口的时候需要在地址后面增加两个参数<b>/{DirectoryName}/{FileName}</b>,其中<b>DirectoryName</b>代表存放的目录,<b>FileName</b>代表文件名,上传成功接口返回201状态码，并且body中会返回下载接口和删除接口需要访问的地址后缀,使用者需要在基础的前缀地址上拼接上此接口地址后缀，来查看(Get)或者删除(Delete)文件
```json
    {
        "requestPath": "big/tmp/dd.exe"
    }
```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
