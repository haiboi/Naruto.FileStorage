﻿using Microsoft.AspNetCore.Http;
using Naruto.FileStorage.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.FileStorage.Object
{
    /// <summary>
    /// 配置信息
    /// </summary>
    public class FileStorageOption
    {
        /// <summary>
        /// 请求地址
        /// 默认的请求前缀为 /naruto/file
        /// </summary>
        public PathString RequestPath { get; set; } = new PathString("/naruto/file");

        /// <summary>
        /// 扩展
        /// </summary>
        internal List<IFileStorageOptionExtension> Extensions { get; set; }

        public FileStorageOption()
        {
            Extensions = new List<IFileStorageOptionExtension>();
        }
        /// <summary>
        /// 注册扩展
        /// </summary>
        /// <param name="fileStorageOptionExtension"></param>
        public void RegisterExtension(IFileStorageOptionExtension fileStorageOptionExtension)
        {
            Extensions.Add(fileStorageOptionExtension);
        }
    }
}
