﻿using Microsoft.Extensions.DependencyInjection;
using Minio;
using Naruto.FileStorage.Abstractions;
using Naruto.FileStorage.Interface;
using Naruto.FileStorage.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.FileStorage.Provider.MinIO
{
    /// <summary>
    /// 
    /// </summary>
    internal class FileStorageOptionExtension : IFileStorageOptionExtension
    {
        private Action<MinIOOption> _action;
        public FileStorageOptionExtension(Action<MinIOOption> action)
        {
            _action = action;
        }

        public void AddService(IServiceCollection services)
        {
            services.Configure(_action);
            services.AddSingleton<IFileStorage, MinIOFileStorage>();
        }
    }
    public static class MinIOFileStorageOptionExtension
    {
        public static void UseMinIO(this FileStorageOption fileStorageOption, Action<MinIOOption> action)
        {
            fileStorageOption.RegisterExtension(new FileStorageOptionExtension(action));
        }
    }
}
