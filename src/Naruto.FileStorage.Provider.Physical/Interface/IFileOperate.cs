﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.Infrastructure.Interface
{
    /// <summary>
    /// 文件操作接口
    /// </summary>
    public interface IFileOperate
    {
        /// <summary>
        /// 处理一段文件流上传至服务器的逻辑
        /// </summary>
        /// <param name="uploadFIleStream">上传的文件流</param>
        /// <param name="uploadFilePath">服务器（本地）目标地址</param>
        Task UpLoadFileFromStream(Stream uploadFIleStream, string uploadFilePath);


        /// <summary>
        /// 根据物理路径获取资源信息
        /// </summary>
        /// <param name="resourcePath">资源地址</param>
        /// <returns></returns>
        Task<byte[]> GetFileAsync(string resourcePath);

        /// <summary>
        /// 文件地址的删除
        /// </summary>
        /// <param name="resourcePath"></param>
        /// <returns></returns>
        Task DeleteFileAsync(string resourcePath);
    }
}
