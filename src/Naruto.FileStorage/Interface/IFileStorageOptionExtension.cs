﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.FileStorage.Interface
{
    /// <summary>
    /// 文件存储扩展
    /// </summary>
    public interface IFileStorageOptionExtension
    {
        /// <summary>
        /// 注册服务
        /// </summary>
        /// <param name="services"></param>
        void AddService(IServiceCollection services);
    }
}
