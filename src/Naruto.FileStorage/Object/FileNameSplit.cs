﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.FileStorage.Object
{
    public class FileNameSplit
    {

        /// <summary>
        /// 目录名
        /// </summary>
        public string DirectoryName { get; set; }


        /// <summary>
        ///文件名  
        /// </summary>
        public string FileName { get; set; }

        public override string ToString()
        {
            return $"/{DirectoryName}/{FileName}";
        }
    }
}
