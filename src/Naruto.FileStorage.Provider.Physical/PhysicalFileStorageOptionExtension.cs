﻿using Microsoft.Extensions.DependencyInjection;
using Naruto.FileStorage.Abstractions;
using Naruto.FileStorage.Interface;
using Naruto.FileStorage.Object;
using Naruto.FileStorage.Provider.Physical.Internal;
using Naruto.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.FileStorage.Provider.Physical
{
    internal class FileStorageOptionExtension : IFileStorageOptionExtension
    {
        private Action<PhysicalFileOptions> _action;
        public FileStorageOptionExtension(Action<PhysicalFileOptions> action)
        {
            _action = action;
        }
        public void AddService(IServiceCollection services)
        {
            services.AddSingleton<IFileOperate, FileOperate>();
            services.AddSingleton<IFileStorage, PhysicalFileStorage>();
            services.Configure(_action);
        }
    }

    public static class PhysicalFileStorageOptionExtensions
    {
        /// <summary>
        /// 从物理磁盘操作文件
        /// </summary>
        /// <param name="services"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static void UsePhyscalFileStorage(this FileStorageOption option, Action<PhysicalFileOptions> action)
        {
             option.RegisterExtension(new FileStorageOptionExtension(action));
        }
        /// <summary>
        /// 从物理磁盘操作文件
        /// </summary>
        /// <param name="services"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static void UsePhyscalFileStorage(this FileStorageOption option)
        {
            option.RegisterExtension(new FileStorageOptionExtension((s) => { }));
        }
    }
}
