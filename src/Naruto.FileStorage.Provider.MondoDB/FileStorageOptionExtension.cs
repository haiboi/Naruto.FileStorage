﻿using Microsoft.Extensions.DependencyInjection;
using Naruto.FileStorage.Abstractions;
using Naruto.FileStorage.Interface;
using Naruto.FileStorage.Object;
using Naruto.FileStorage.Provider.MongoDB.Interface;
using Naruto.FileStorage.Provider.MongoDB.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naruto.FileStorage.Provider.MongoDB
{
    internal class FileStorageOptionExtension : IFileStorageOptionExtension
    {
        private Action<MongoDBFileStorageContext> _context;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public FileStorageOptionExtension(Action<MongoDBFileStorageContext> context)
        {
            _context = context;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public void AddService(IServiceCollection services)
        {
            services.AddScoped<IMongoDBFile, MongoDBFile>();
            services.AddScoped<IFileStorage, MongoDBFileStorage>();
            //注入mongo存储
            services.AddMongoServices()
                .AddMongoContext(_context);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public static class MongoFileStorageOptionExtension
    {
        /// <summary>
        /// 从mongodb操作文件
        /// </summary>
        /// <param name="services"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static void UseMondbDBFileStorage(this FileStorageOption option, Action<MongoDBFileStorageContext> context)
        {
            option.RegisterExtension(new FileStorageOptionExtension(context));
        }
    }
}
