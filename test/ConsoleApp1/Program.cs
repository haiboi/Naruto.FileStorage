﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Naruto.FileStorage;
using Naruto.FileStorage.Provider.MongoDB;
using Naruto.FileStorage.Provider.Physical;
using System;
using System.IO;
using System.Threading.Tasks;
using WebApplication1;

namespace ConsoleApp1
{
    public class Program
    {
        static async Task Main(string[] args)
        {

            var build = Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webHost =>
                {
                    //配置监听端口
                    webHost.UseKestrel(option =>
                    {
                        option.ListenAnyIP(5000);
                        option.Limits.MaxRequestBodySize = int.MaxValue;
                        option.Limits.MaxRequestBufferSize = int.MaxValue;
                    });
                    //webHost.ConfigureAppConfiguration((a, b) =>
                    //{
                    //    b.AddJsonFile(Path.Combine(AppContext.BaseDirectory, "appsetting.json"), false, true);
                    //});
                    ////添加服务
                    //webHost.ConfigureServices(services =>
                    //{
                    //    var configuration = services.BuildServiceProvider().GetRequiredService<IConfiguration>();
                    //    services.AddFileStorage(option =>
                    //    {

                    //    })
                    //    //.AddPhyscalFileStorage()
                    //    .AddMondbDBFileStorage(a =>
                    //    {
                    //        a.ConnectionString = configuration.GetSection("mongodb").GetValue<string>("dbstring");
                    //        a.DataBase = configuration.GetSection("mongodb").GetValue<string>("database");
                    //    });
                    //    var cors = configuration.GetSection("cors").Get<string[]>();
                    //    services.AddCors(a =>
                    //    {
                    //        a.AddDefaultPolicy(option =>
                    //        {
                    //            option.AllowCredentials();
                    //            option.AllowAnyHeader();
                    //            option.AllowAnyMethod();
                    //            option.WithOrigins(cors);
                    //        });
                    //    });
                    //});
                    ////配置管道中间件
                    //webHost.Configure(option =>
                    //{
                    //    option.UseRouting();
                    //    option.UseCors();
                    //    option.UseFileStorageMiddleware();
                    //    option.UseEndpoints(endpoints =>
                    //    {
                    //        endpoints.MapGet("/", async context =>
                    //        {
                    //            await context.Response.WriteAsync("Hello World!");
                    //        });
                    //    });
                    //});
                    webHost.UseStartup<Startup>();
                })
                   .Build();
            await build.RunAsync();
        }
    }
}
