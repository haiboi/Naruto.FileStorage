﻿using Naruto.FileStorage.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace Naruto.FileStorage.Helper
{
    /// <summary>
    /// 对文件名进行拆分
    /// </summary>
    public static class FileNameSplitHelper
    {
        /// <summary>
        /// 根据 / 进行拆分 获取目录和文件名
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static FileNameSplit SplitFileName(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return default;
            }
            var res = str.Split('/', StringSplitOptions.RemoveEmptyEntries);
            if (res != null && res.Length < 2)
            {
                return default;
            }
            return new FileNameSplit
            {
                DirectoryName = res[0],
                FileName = res[1]
            };
        }
    }
}
